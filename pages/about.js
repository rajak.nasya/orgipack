import React from 'react';
import Head from 'next/head';
import Root from '../components/common/Root';
import Footer from '../components/common/Footer';

const About = () => (
  <Root>
    <Head>
      <title>About | commerce</title>
    </Head>
    <div className="about-container">
      {/* Row */}
      <div className="row mt-5 pt-5 about-hero">
        <div className="col-12 col-md-10 col-lg-12 offset-md-1 offset-lg-0 row-content">
          <div className="h-100 d-flex flex-column py-5 px-4 px-sm-5 justify-content-center">
            <h2 className="font-size-header mb-4">
              We’re making world to adopt to an eco-friendly green lifestyle
            </h2>
            <h4 className="font-size-subheader mb-2">
              Orgipack provide quality and sustainable food packaging solutions and services to customers around the Ireland and Europe. Our company is providing disposable environmentally friendly tableware and packaging products with a full range of lunch boxes, trays, plates, bowls, etc. made from natural sugarcane fiber or bamboo fiber relying on high technology.
            </h4>
            <h4 className="font-size-subheader mb-3">
              We can provide all different shapes of food packaging solutions made of bagasse pulp and bamboo pulp plant-based fibre which are processed professionally which are environment friendly with the advantages of being organic, compostable, biodegradable, nontoxic, good toughness, heat resistant and eco-friendly. Our food packaging range includes food service packaging, packaging use in restaurants and takeaway.
              Orgipack also offers a full custom branding and bespoke packaging service thereby providing customers with tailored solutions that meet both their brand and environmental ambitions and objectives.
            </h4>
            <div>
              <h2 className="font-size-header mb-1">OUR MISSION: </h2>
              <h4 className="font-size-subheader mb-2">To make a world free from Plastic waste.</h4>
              <h2 className="font-size-header mb-1">OUR VISION:</h2>
              <h4 className="font-size-subheader mb-2">To be world-class packaging solution provider.</h4>
              <h2 className="font-size-header mb-1">OUR VALUES:</h2>
              <h4 className="font-size-subheader mb-4">Customer First, Teamwork, Embracing Change, Integrity, Keeping Passion and Dedication.</h4>
            </div>
            <h2 className="font-size-header mb-2">
              SUSTAINABILITY
            </h2>
            
            <h4 className="font-size-subheader mb-1">
              At Orgipack, we have a mission to deliver sustainable packaging solution to protect our only planet earth. And our proficiency lies in finding the most practical and reliable solution to help customers switch to sustainable mode. Commitment to sustainable development is fundamental to long-term success.
            </h4>
            <h4 className="font-size-subheader mb-4">

              Furthermore, our thirst for consumer products is creating a throw away society which is polluting our environments on a global scale. From rising landfill mountains to toxic seas, our natural habitats are dying under the sheer rate and volume of consumer consumption. Through simple measures through the use of compostable, biodegradable, recyclable and reusable products we can make a significant impact to reduce this unsustainable level of degradation.

              .
            </h4>
            {/* <div className="col-12 col-lg-12"> */}
            <div className="about-image h-100 mb-3">
              <div className="d-flex align-items-center justify-content-center h-100">
                <img src="/images/ocean-image.png" alt="About Ocean" width={800} />
              </div>
            </div>
            {/* </div> */}
            <h4 className='font-size-subheader mb-1'>
              Why Choose Sustainable Packaging?
            </h4>
            <h4 className='font-size-subheader mb-2'>
              Whether you are looking for bags, boxes or cups and other packaging– you can make a responsible choice with sustainable products from Orgipack.
            </h4>
            <h4 className='font-size-subheader mb-4'>
              Sustainable packaging is important because it reduces the waste and pollution of all the stages in the products life-cycle. It also helps both the product and the consumer reduce their environmental impact. Brands image can be effectively enhanced when packaging reminds people’s environmental awareness.
            </h4>
            <h4 className='font-size-header mb-1'>
              Our diverse range lets you choose from products which are:
            </h4>
            <ul className='mb-4'>
              <li style={{ color: 'green' }}>Compostable</li>
              <li style={{ color: 'green' }}>Sustainable</li>
              <li style={{ color: 'green' }}>Biodegradable </li>
            </ul>
            <h4 className='font-size-header mb-1'>
              Products:
            </h4>
            <h4 className='font-size-subheader mb-1'>
              Category 1 : Takeaway Boxes<br />
              Category 2: Lunch Boxes<br />
              Category 3 : Hot Cup lids<br />
              Category 4 : Sauce Cups ( see excel sheet for product details)
            </h4>

          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-md-12 col-lg-12">
          <div className="about-image h-100">
            <div className="d-flex justify-content-center h-100">
              <div className='col-md-8'>
                <h2 className='font-size-header mb-1 text-center'>
                  Drop A Line
                </h2>

                <h5 className='font-size-subheader mb-1 text-center'>
                  Thinking about going solar? Find out how much youll save.
                </h5>
                <div className="row">
                  <div className="col-12 col-sm-6 mb-3">
                    <label className="w-100">
                      <p className="mb-1 font-size-caption font-color-light">
                        Full Name
                      </p>
                      <input required name="customer[first_name]" autoComplete="given-name" className="rounded-0 w-100" />
                    </label>
                  </div>
                  <div className="col-12 col-sm-6 mb-3">
                    <label className="w-100">
                      <p className="mb-1 font-size-caption font-color-light">
                        Email Address
                      </p>
                      <input required name="customer[last_name]" autoComplete="family-name" className="rounded-0 w-100" />
                    </label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-6 mb-3">
                    <label className="w-100">
                      <p className="mb-1 font-size-caption font-color-light">
                        Phone No.
                      </p>
                      <input required name="customer[first_name]" autoComplete="given-name" className="rounded-0 w-100" />
                    </label>
                  </div>
                  <div className="col-12 col-sm-6 mb-3">
                    <label className="w-100">
                      <p className="mb-1 font-size-caption font-color-light">
                        Eircode
                      </p>
                      <input required name="customer[last_name]" autoComplete="family-name" className="rounded-0 w-100" />
                    </label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-12 mb-3">
                    <label className="w-100">
                      <p className="mb-1 font-size-caption font-color-light">
                        Your Message
                      </p>
                      <textarea required name="customer[first_name]" autoComplete="given-name" className="rounded-0 w-100" ></textarea>
                    </label>
                  </div>
                </div>
                <div className='float-right'>
                  <button type="submit"
                    className="bg-black font-color-white w-100 border-none h-40 font-weight-semibold d-lg-block">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-md-12 col-lg-12">
          <div className="about-image h-100">
            <div className="h-100 d-flex flex-column py-5 px-4 px-sm-5 justify-content-center">
              <h4 className='font-size-header mb-1'>
                Contact us
              </h4>
              Office 308 & 311 Airport House, Shannon Free Zone, Shannon, Co. Clare<br/>
              +353 61 719 730

            </div>
          </div>
        </div>
      </div>
      {/* Row 3 */}
      {/* <div className="row">
        <div className="col-12 col-md-10 col-lg-6 offset-md-1 offset-lg-0 row-content">
          <div className="h-100 d-flex flex-column justify-content-center py-5 px-4 px-sm-5">
            <h3 className="font-size-header mb-4">
              Clone in GitHub
            </h3>
            <h4 className="font-size-subheader mb-4">
            If you would like to clone this project and do a manual setup, go to the repository below!
            </h4>
            <div className="mt-3">
              <a
                className="px-4 py-3 flex-grow-1 font-color-white about-gb"
                href="https://github.com/chec/commercejs-nextjs-demo-store"
                target="_blank"
                rel="noopener noreferrer"
              >
                Clone in GitHub
              </a>
            </div>
          </div>
        </div>

        <div className="col-12 col-lg-6">
          <div className="about-image h-100">
            <div className="d-flex align-items-center justify-content-center h-100">
              <img src="/github-illustration.svg" alt="GitHub illustration"/>
            </div>
          </div>
        </div>
      </div> */}

      {/* Row 4 */}
      {/* <div className="row">
        <div className="col-12 col-lg-6">
          <div className="about-image h-100">
            <div className="d-flex align-items-center justify-content-center h-100">
              <img src="/blog-illustration.svg" alt="Commerce.js blog illustration"/>
            </div>
          </div>
        </div>

        <div className="col-12 col-md-10 col-lg-6 offset-md-1 offset-lg-0 row-content">
          <div className="h-100 d-flex flex-column py-5 px-4 px-sm-5">
            <h3 className="font-size-header mb-4">
              Technical Blog
            </h3>
            <h4 className="font-size-subheader mb-4">
              Read the full technical tutorial here if you want to get into the nitty gritty!
            </h4>
            <div className="mt-3">
              <a
                className="px-5 py-3 flex-grow-1 font-color-white about-blog"
                href="https://www.netlify.com/blog/2020/07/09/create-a-fully-fledged-jamstack-commerce-store-with-commerce.js-and-netlify/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Read Blog
              </a>
            </div>
          </div>
        </div>
      </div> */}
    </div>
    <Footer />
  </Root>
);

export default About;
